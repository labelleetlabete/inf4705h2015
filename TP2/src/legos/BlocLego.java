package legos;


public class BlocLego {
	public int hauteur;
	public int largeur;
	public int profondeur;

	private void arrangerLargeurEtProfondeur() {
		if(largeur > profondeur){
			int temp = largeur;
			largeur =profondeur;
			profondeur = temp;
		}
	}

	public BlocLego(int h,int l, int p){
		hauteur = h;
		largeur = l;
		profondeur = p;
		arrangerLargeurEtProfondeur();
		
	}
	
	public int getSurface(){
		return largeur * profondeur;
	}

	public boolean peutSupporter(BlocLego bL) {
		return largeur > bL.largeur && profondeur > bL.profondeur;
	}
	
	@Override
    public String toString() {
		String str = "H = %d L = %s P = %d S = %d";
		String formatted = String.format(str,hauteur,largeur,profondeur,getSurface());

		return formatted;
    }
	
	@Override
    public boolean equals(Object object)
    {
        boolean sameSame = false;

        if (object != null && object instanceof BlocLego)
        {
        	BlocLego bl2 = (BlocLego)object;
            sameSame = this.largeur == bl2.largeur &&
            		this.hauteur == bl2.hauteur &&
            				this.profondeur == bl2.profondeur;
        }

        return sameSame;
    }
	
	@Override
	public int hashCode(){
		return largeur * (hauteur + profondeur) + profondeur * (hauteur + largeur);
	}


}
