/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sorters;

import Interfaces.ISortable;
import Tools.Timer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * base sur: http://rosettacode.org/wiki/Sorting_algorithms/Merge_sort#Java
 *
 * @author bepala
 */
public class MergeSort implements ISortable {
    public static int THRESHOLD = 1;
    
    
    private List<Integer> mergeSort(List<Integer> m) {
        if (m.size() <= 1) { //O(1)
            return m;
        }
        else if (m.size() <= THRESHOLD) { //O(1)
            SelectionSort sorter = new SelectionSort();            
            sorter.Sort(m);
            
            return m;
        }
        
        int middle = m.size() / 2; //O(1)
        List<Integer> left = m.subList(0, middle); //O(1)
        List<Integer> right = m.subList(middle, m.size()); //O(1)

        right = mergeSort(right); //T(⌊(n − 1)/2⌋)
        left = mergeSort(left); //T(⌊(n − 1)/2⌋)
        List<Integer> result = merge(left, right); //O(n)

        return result;
    }

    private List<Integer> merge(List<Integer> left, List<Integer> right) {
        List<Integer> result = new ArrayList<>();
        Iterator<Integer> it1 = left.iterator();
        Iterator<Integer> it2 = right.iterator();

        Integer x = it1.next();
        Integer y = it2.next();
        while (true) {
            //change the direction of this comparison to change the direction of the sort
            if (x.compareTo(y) <= 0) {
                result.add(x);
                if (it1.hasNext()) {
                    x = it1.next();
                } else {
                    result.add(y);
                    while (it2.hasNext()) {
                        result.add(it2.next());
                    }
                    break;
                }
            } else {
                result.add(y);
                if (it2.hasNext()) {
                    y = it2.next();
                } else {
                    result.add(x);
                    while (it1.hasNext()) {
                        result.add(it1.next());
                    }
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public long Sort(int[] arrayToSort) {
        List<Integer> m = new ArrayList<>(arrayToSort.length);

        for (int i = 0; i < arrayToSort.length; ++i) {
            m.add(arrayToSort[i]);
        }

        Timer t = new Timer();
        t.Start();

        m = mergeSort(m); //O(n log n)

        t.Stop();

        for (int i = 0; i < arrayToSort.length; ++i) {
            arrayToSort[i] = m.get(i);
        }

        return t.getElapsedTimeMicroS();

    }

}
