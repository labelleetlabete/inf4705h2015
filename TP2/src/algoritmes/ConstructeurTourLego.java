package algoritmes;

import legos.BoiteALego;
import legos.Tour;

public abstract  class ConstructeurTourLego {
	public abstract Tour Construire(BoiteALego boite);
	
	public boolean verbose;
	protected void Explique(String string) {
		if(verbose){
			//System.out.println(string);
		}
		
	}
	
	public ConstructeurTourLego(boolean verbose){
		this.verbose = verbose;
	}
}
