package legos;

import java.io.IOException;

import Tools.IOUtils;
import Tools.RandomHelper;
import algoritmes.ConstructeurDynamique;
import algoritmes.ConstructeurRecuitSimule;
import algoritmes.ConstructeurTourLego;
import algoritmes.ConstructeurVorace;

public class ArchitecteDynamique {
	public void Archi() {
		String folderPath = "Data/Dataset";
		String resultatsHauteur = "resultats.txt";
		//String resultatsTemps = "resultats.txt";
		String folderResultat = "Data/Resultats/Dynamique";
		//IOUtils.create(folderResultat, resultatsHauteur);
		//IOUtils.create(folderResultat, resultatsTemps);

		String[] grosseurs = new String[] { "100", "500", "1k", "5k",
				"10k", "50k", "100k" };
		String data[];
		BoiteALego bAL;
		Tour t;
		long duree;
		int hauteur;
		
		for (int i =0; i < 5; ++i){
			String grosseur = grosseurs[i];
			hauteur =0;
			duree =0;
			for (int set = 0; set < 10; ++set) {

				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseur + "_"
							+ set);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					ConstructeurDynamique constructeur = new ConstructeurDynamique(false);
				    t = constructeur.Construire(bAL);
				    hauteur += t.getHauteur();
				    duree += t.tempsDeConstruction;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		System.out.println("Debut du vrai truc");
		
		for (int i =grosseurs.length -1; i < grosseurs.length; ++i){
			String grosseur = grosseurs[i];
			hauteur =0;
			duree =0;
			for (int set = 0; set < 10; ++set) {

				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseur + "_"
							+ set);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					ConstructeurDynamique constructeur = new ConstructeurDynamique(false);
				    t = constructeur.Construire(bAL);
				    hauteur += t.getHauteur();
				    duree += t.tempsDeConstruction;
				    System.out.println("Fin de set: " + set + " grosseur: " + grosseur + " hauteur: " + t.getHauteur());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Fin de set: " + set + " grosseur: " + grosseur);

			}
			IOUtils.append(folderResultat, resultatsHauteur, grosseur + " " + hauteur / 10 + " " + duree / 10 + System.lineSeparator());
			//IOUtils.append(folderResultat, resultatsTemps, grosseur + " " + duree / 10 + System.lineSeparator());
		}
	}
}
