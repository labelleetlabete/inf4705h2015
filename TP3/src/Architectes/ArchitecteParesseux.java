package Architectes;

import java.io.IOException;
import java.util.List;

import legos.BoiteALego;
import legos.Tour;
import Tools.IOUtils;
import algoritmes.ConstructeurParesseux;
import algoritmes.ConstructeurToursLego;

public class ArchitecteParesseux implements IArchitecte {

	
	
	public void archi() {

		ConstructeurToursLego cTL = new ConstructeurParesseux(false);
		
		String folderPath = "Data/Dataset";
		String resultatsHauteur = "resultatsParesseux3.txt";
		String folderResultat = "Data/Resultats/Paresseux";
		IOUtils.create(folderResultat, resultatsHauteur);

		String[] grosseurs = new String[] { "100", "500", "1k", "5k",
				"10k", "50k", "100k" };
		int[] grosseursInt = new int[] { 100, 500, 1000, 5000,
				10000, 50000, 100000 };
		String data[];
		BoiteALego bAL;
		List<Tour> t = null;
		
		float count = 0;
		int i =0;
		for (String grosseur : grosseurs) {
			count = 0;
			for (int set = 0; set < 10; ++set) {

				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseur + "_"
							+ set);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					
				    t = cTL.Construire(bAL);
				    boolean valide = bAL.validerTours(t);
				    if(!valide){
				    	System.err.println("SHITHSITHSITHSITHIST!");
				    }
				    count += t.size();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			IOUtils.append(folderResultat, resultatsHauteur, grosseursInt[i] + " " + count / 10f + System.lineSeparator());
			++i;
		}
	}

}
