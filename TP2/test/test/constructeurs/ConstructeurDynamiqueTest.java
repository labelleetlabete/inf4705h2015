package test.constructeurs;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import legos.*;

import org.junit.Test;

import Tools.IOUtils;
import algoritmes.*;

public class ConstructeurDynamiqueTest {

	@Test
	public void test() {
		String folderPath = "donnees";
		String fileName = "b1k_0";
		
		String[] data =null;
		try {
			data = IOUtils.fileToLines(folderPath, fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BoiteALego bAL = new BoiteALego(BoiteALego.dataABloc(data));
		
		ConstructeurTourLego constructeur = new ConstructeurDynamique(false);
		Tour t = constructeur.Construire(bAL);

		assertTrue(Tour.validerTour(t));
	}
	

	

}
