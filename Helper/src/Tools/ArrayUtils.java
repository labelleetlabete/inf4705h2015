/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author bepala
 */
public class ArrayUtils {

    public static void swap(int i, int j, int[] array) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void swap(int i, int j, List<Integer> array) {
        int temp = array.get(i);
        array.set(i, array.get(j));
        array.set(j, temp);
    }

    public static boolean isAscending(int[] array) {
        boolean isAscending = true;

        for (int i = 0; i < array.length - 1 && isAscending; ++i) {
            isAscending = ((array[i + 1]) >= array[i]);
        }

        return isAscending;
    }

    public static int[] StringArrayToIntArray(String[] stringArray) {
        int[] intArray = new int[stringArray.length];

        for (int i = 0; i < stringArray.length; ++i) {
            intArray[i] = Integer.parseInt(stringArray[i]);
        }

        return intArray;
    }

    /**
     * source http://pastebin.com/f57ae8d47
     *
     * @param arrayToSort
     * @return
     */
    public static int max(int[] arrayToSort) {
        int max = Integer.MIN_VALUE;
        for (int value : arrayToSort) {
            if (value > max) {
                max = value;
            }
        }
        return max;

    }

    /**
     * http://pastebin.com/f57ae8d47
     *
     * @param arrayToSort
     * @return
     */
    public static int min(int[] arrayToSort) {
        int min = Integer.MAX_VALUE;
        for (int value : arrayToSort) {
            if (value < min) {
                min = value;
            }
        }
        return min;
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; ++i) {
            System.out.println(array[i]);
        }
    }

}
