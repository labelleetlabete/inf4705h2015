/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Tools;

/**
 *
 * @author bepala
 */
public class Timer {
    
    private long startTime;
    private long endTime;
    
    public Timer(){
        startTime = 0;
        endTime = 0;
    }
    
    public void Start(){
       startTime = System.nanoTime();
       endTime = startTime;
    }
    
    public void Stop(){
        endTime = System.nanoTime();
    }
    
    public long getElapsedTimeMS(){
        return (endTime - startTime) / (long)1000000;
    }
    
        public long getElapsedTimeMicroS(){
        return (endTime - startTime) / (long)1000;
    }
}
