/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sorters;

import Interfaces.ISortable;
import Tools.ArrayUtils;
import Tools.Timer;

/**
 *
 * @author bepala
 */
public class CountingSort implements ISortable {

    @Override
    /**
     * source http://rosettacode.org/wiki/Sorting_algorithms/Counting_sort#Java
     */
    public long Sort(int[] arrayToSort) {
        int min = ArrayUtils.min(arrayToSort);
        int max = ArrayUtils.max(arrayToSort);

        Timer t = new Timer();
        t.Start();

        
        int[] count = new int[max - min + 1]; //O(1)
        
        for (int number : arrayToSort) { //O(n)
            count[number - min]++;
        }
        int z = 0;
        for (int i = min; i <= max; i++) {  //O(max - min) -> O(k)
            while (count[i - min] > 0) {  //On rentre dans le while n fois donc le for devient O(n + k)
                arrayToSort[z] = i;
                z++;
                count[i - min]--;
            }
        }
        
        //On a donc au total O(2n + k) -> O(n + k)

        t.Stop();
        return t.getElapsedTimeMicroS();
    }

    public static void countingSort(int[] array, int min, int max) {
        int[] count = new int[max - min + 1];
        for (int number : array) {
            count[number - min]++;
        }
        int z = 0;
        for (int i = min; i <= max; i++) {
            while (count[i - min] > 0) {
                array[z] = i;
                z++;
                count[i - min]--;
            }
        }
    }

}
