package legos;

import java.io.IOException;

import Tools.IOUtils;
import Tools.RandomHelper;
import algoritmes.ConstructeurDynamique;
import algoritmes.ConstructeurRecuitSimule;
import algoritmes.ConstructeurTourLego;
import algoritmes.ConstructeurVorace;

public class ArchitecteRC {
	public void Archi() throws IOException {
		int nbPas = 728;
		String folderPath = "Data/Dataset";
		String resultats = "resultats.txt";
		String folderResultat = "Data/Resultats/RecuitSimule";
		IOUtils.create(folderResultat, resultats);

		String[] grosseurs = new String[] { "100", "500", "1k", "5k", "10k",
				"50k", "100k" };
		int[] grosseursInt = new int[] { 100, 500, 1000, 5000, 10000, 50000,
				100000 };
		String data[];
		BoiteALego bAL;
		Tour t;
		long duree;
		int hauteur = 0;
		
		//briser l'optimisation
		int grosseurIntTemp =0;
		for (int j = 0; j < 5; ++j) {
			String grosseurS = grosseurs[j];
			hauteur = 0;
			duree = 0;
			grosseurIntTemp =0;
			for(int i =0; i < 10; ++i){
				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseurS + "_"
							+ i);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					ConstructeurRecuitSimule constructeur = new ConstructeurRecuitSimule(
							false, nbPas, 150, (int)(grosseursInt[grosseurIntTemp] * 1.75f), 0.99f);
				    t = constructeur.Construire(bAL);
				    hauteur += t.getHauteur();
				    duree += t.tempsDeConstruction;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		

		int grosseurInt =0;
		for (String grosseurS : grosseurs) {
			hauteur = 0;
			duree = 0;
			for(int i =0; i < 10; ++i){
				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseurS + "_"
							+ i);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					ConstructeurRecuitSimule constructeur = new ConstructeurRecuitSimule(
							false, nbPas, 150, (int)(grosseursInt[grosseurInt] * 1.75f), 0.99f);
				    t = constructeur.Construire(bAL);
				    hauteur += t.getHauteur();
				    duree += t.tempsDeConstruction;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			IOUtils.append(folderResultat, resultats, grosseurs[grosseurInt] + " " + hauteur / 10 + " " + duree / 10 + System.lineSeparator());
			grosseurInt++;
			
		}
	}
}
