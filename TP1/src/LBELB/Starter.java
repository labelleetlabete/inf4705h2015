/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LBELB;

import Interfaces.ISortable;
import Sorters.CountingSort;
import Sorters.MergeSort;
import Sorters.SelectionSort;
import Tools.ArrayUtils;
import Tools.IOUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bepala
 */
public class Starter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ISortable sorter = new MergeSort();
        MergeSort.THRESHOLD = 25;
        int[] longueursSet = {1000, 5000, 10000, 50000, 100000, 500000};
        
        //loop de rechauffemend
        for (int i = 0; i < 2; ++i) {
            
            String fichierResultat = "resultats_" + i + ".txt";
            IOUtils.create(RESULTS_FOLDER, fichierResultat);
            for (int longueur : longueursSet) {
                
                long tempsTotal = 0;
                
                for (int j = 0; j < 10; ++j) {

                    String fichierJeuDeTest = "testset_" + longueur + "_" + (i * 10 + j) + ".txt";
                    int[] jeuDeTest;
                    try {
                        jeuDeTest = ArrayUtils.StringArrayToIntArray(IOUtils.fileToLines(DATA_FOLDER, fichierJeuDeTest));
                        tempsTotal += sorter.Sort(jeuDeTest);                     
                    } catch (IOException ex) {
                        Logger.getLogger(Starter.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                
                IOUtils.append(RESULTS_FOLDER, fichierResultat, longueur + " " + tempsTotal / 10 + System.lineSeparator());
                Logger.getLogger(Starter.class.getName()).log(Level.INFO, (longueur + ":" + i + " done"));
            }
        }
        
        
        for (int i = 0; i < 2; ++i) {
            
            String fichierResultat = "resultats_" + i + ".txt";
            IOUtils.create(RESULTS_FOLDER, fichierResultat);
            for (int longueur : longueursSet) {
                
                long tempsTotal = 0;
                
                for (int j = 0; j < 10; ++j) {

                    String fichierJeuDeTest = "testset_" + longueur + "_" + (i * 10 + j) + ".txt";
                    int[] jeuDeTest;
                    try {
                        jeuDeTest = ArrayUtils.StringArrayToIntArray(IOUtils.fileToLines(DATA_FOLDER, fichierJeuDeTest));
                        tempsTotal += sorter.Sort(jeuDeTest);                     
                    } catch (IOException ex) {
                        Logger.getLogger(Starter.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                
                IOUtils.append(RESULTS_FOLDER, fichierResultat, longueur + " " + tempsTotal / 10 + System.lineSeparator());
                Logger.getLogger(Starter.class.getName()).log(Level.INFO, (longueur + ":" + i + " done"));
            }
        }
        
//        //test pour le facteur k
//        for (int i = 0; i < 3; ++i) {
//            String fichierResultat = "resultats_facteurKIndividuel_" + i + ".txt";
//            IOUtils.create(RESULTS_FOLDER, fichierResultat);
//            for (int longueur : longueursSet) {
//                
//                long kTotal = 0;
//                
//                for (int j = 0; j < 10; ++j) {
//kTotal = 0;
//                    String fichierJeuDeTest = "testset_" + longueur + "_" + (i * 10 + j) + ".txt";
//                    int[] jeuDeTest;
//                    try {
//                        jeuDeTest = ArrayUtils.StringArrayToIntArray(IOUtils.fileToLines(DATA_FOLDER, fichierJeuDeTest));
//                        kTotal += ArrayUtils.max(jeuDeTest) - ArrayUtils.min(jeuDeTest);
//                        IOUtils.append(RESULTS_FOLDER, fichierResultat, longueur + "_" + i * 10 + j + " "  + kTotal + System.lineSeparator());
//
//                    } catch (IOException ex) {
//                        Logger.getLogger(Starter.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//
//                }
//                
//                Logger.getLogger(Starter.class.getName()).log(Level.INFO, (longueur + ":" + i + " done"));
//            }
//        }
//        {
//        int longueur = 500000;
//            String fichierResultat = "resultats_thres_" + longueur + ".txt";
//            IOUtils.create(RESULTS_FOLDER, fichierResultat);
//            for (int thres = 1; thres < 101; ++thres) {
//                MergeSort.THRESHOLD = thres;
//
//                long tempsTotal = 0;
//
//                for (int j = 0; j < 10; ++j) {
//
//                    String fichierJeuDeTest = "testset_" + longueur + "_" + (j) + ".txt";
//                    int[] jeuDeTest;
//                    try {
//                        jeuDeTest = ArrayUtils.StringArrayToIntArray(IOUtils.fileToLines(DATA_FOLDER, fichierJeuDeTest));
//                        tempsTotal += sorter.Sort(jeuDeTest);
//                    } catch (IOException ex) {
//                        Logger.getLogger(Starter.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//
//                }
//
//                IOUtils.append(RESULTS_FOLDER, fichierResultat, thres + " " + tempsTotal / 10 + System.lineSeparator());
//                Logger.getLogger(Starter.class.getName()).log(Level.INFO, (thres + ":" + "done"));
//            }
//        }

    }
    private static final String DATA_FOLDER = "Data//";
    private static final String RESULTS_FOLDER = "Data//Resultats//MergeSort//Threshold25//";

}
