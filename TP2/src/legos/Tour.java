package legos;

import java.util.ArrayList;

public class Tour {
	//public int hauteur = 0;
	public int getHauteur(){
//		int temp = 0;
//		for(BlocLego bL: blocs){
//			temp += bL.hauteur;
//		}
//		if(hauteur != temp){
//			System.err.println("Hauteur not sync");
//		}
//		return temp;
		return hauteur;
	}
	
	private int hauteur =0;
	
	BlocLego top;
	public BlocLego derniereInsertion;
	public ArrayList<BlocLego> blocsInstables = new ArrayList<>();
	public ArrayList<BlocLego> blocs = new ArrayList<BlocLego>();
	public long tempsDeConstruction = 0;

	public Tour(Tour tourTemp) {
		blocs.addAll(tourTemp.blocs);
		hauteur = tourTemp.hauteur;
		top = tourTemp.top;
	}
	
	public Tour(){
		
	}

	public boolean ajouterBlocLego(BlocLego bL) {
		boolean aEteAjoute = false;
		if (top == null || top.peutSupporter(bL)) {
			blocs.add(bL);
			hauteur += bL.hauteur;
			top = bL;
			aEteAjoute = true;
		}

		return aEteAjoute;
	}

	@Override
	public String toString() {
		String str = "Temps(us) = %d Nombre de blocs = %d Hauteur = %d ";
		String formatted = String.format(str,tempsDeConstruction, blocs.size(), getHauteur() );

		return formatted;
	}

	public void decrisToi(boolean decrireBloc) {
		System.out.println(this.toString());
		if(decrireBloc){
		System.out.println("Voici mes blocs");
		for (BlocLego bL : blocs) {
			System.out.println(bL.toString());
		}
		}

	}

	public void remplacerBase(Tour tour) {
		top = this.blocs.get(this.blocs.size()-1);
		blocs = new ArrayList<>(tour.blocs);
		hauteur = tour.hauteur;
		blocs.add(top);
		hauteur += top.hauteur;

	}

	public void inserer(BlocLego blocAInserer, BoiteALego boite) {
		derniereInsertion = blocAInserer;
		blocsInstables = new ArrayList<>();
		BlocLego blocTemp;
		boolean aInsere = false;
		int dessousIdx = -1;
		for (int i = blocs.size() - 1; i >= 0; --i) {
			blocTemp = blocs.get(i);
			if (blocTemp.peutSupporter(blocAInserer)) {
				blocs.add(i +1, blocAInserer);
				aInsere = true;
				dessousIdx = i +1;
				break;
			}
		}
		//on doit prendre compte des deux dessous possible
		if (aInsere == false) {
			dessousIdx = 0;
			blocs.add(0, blocAInserer);
		}
		hauteur += blocAInserer.hauteur;

		BlocLego dessous = blocs.get(dessousIdx);
		if (blocs.size() > dessousIdx + 1) {
			BlocLego dessus = blocs.get(dessousIdx + 1);
			// on v�rifie si la tour est stable
			while (dessous.peutSupporter(dessus) == false && blocs.size() > dessousIdx) {
				blocsInstables.add(blocs.remove(dessousIdx +1));

				hauteur -= dessus.hauteur;

				if (dessousIdx + 1 >= blocs.size()) {
					break; // on a ote le dernier element possible
				}
				dessus = blocs.get(dessousIdx + 1);
			}
		}
		top = blocs.get(blocs.size() - 1);
	}
	
	public static boolean validerTour(Tour t){
		boolean valide = true;
		int invalidCount = 0;
		for(int i =0; i < t.blocs.size() -1; ++i){
			if(t.blocs.get(i).peutSupporter(t.blocs.get(i+1)) == false){
				valide = false;
				//break;
				++invalidCount;
			}
		}
		
		return valide;
	}

}
