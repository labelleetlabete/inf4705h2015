package legos;

import java.io.IOException;

import Architectes.ArchitecteComplique;
import Architectes.ArchitecteParesseux;
import Architectes.IArchitecte;

public class Architecte {

	public static void main(String[] args) throws IOException {

		IArchitecte archi = new ArchitecteComplique();
		archi.archi();
	}

}
