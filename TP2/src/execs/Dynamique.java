package execs;

import java.io.IOException;

import legos.BoiteALego;
import legos.Tour;
import Tools.IOUtils;
import algoritmes.ConstructeurDynamique;
import algoritmes.ConstructeurTourLego;
import algoritmes.ConstructeurVorace;

public class Dynamique {

	public static void main(String[] args) {
		boolean verbose = false;
		String emplacement ="";
		if(args.length > 1){
			for(int i =0; i < args.length; ++i){
				String arg = args[i];
				
				switch(arg){
				case "-p":
					verbose = true;
					break;
				case "-f":
					emplacement = args[i+1];
					++i;
					break;
				default:
				}
				

				
			}
			String data[];
			try {
				data = IOUtils.fileToLines(emplacement, "");
				BoiteALego bAL = new BoiteALego(BoiteALego.dataABloc(data));
				ConstructeurTourLego constructeur = new ConstructeurDynamique(false);
			    Tour t = constructeur.Construire(bAL);
			    
			    t.decrisToi(verbose);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			System.out.println("Il vous manque des arguments");
		}

	}

}
