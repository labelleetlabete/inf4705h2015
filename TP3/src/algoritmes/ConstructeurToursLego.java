package algoritmes;

import java.util.List;

import legos.BoiteALego;
import legos.Tour;

public abstract  class ConstructeurToursLego {
	public abstract List<Tour> Construire(BoiteALego boite);
	
	public boolean verbose;
	protected void Explique(String string) {
		if(verbose){
			System.out.println(string);
		}
		
	}
	
	public ConstructeurToursLego(boolean verbose){
		this.verbose = verbose;
	}
}
