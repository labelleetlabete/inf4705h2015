package legos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import Tools.RandomHelper;

public class BoiteALego {
	public ArrayList<BlocLego> contenu;
	
	public BoiteALego(ArrayList<BlocLego> legos)
	{
		contenu = legos;
	}
	
	public void OrdonnerLego(){
		Collections.sort(contenu, new LegoComparator());
		Collections.reverse(contenu);
	}
	
	private class LegoComparator implements Comparator<BlocLego>{

		@Override
		public int compare(BlocLego lego1, BlocLego lego2) {			
			int comparaison = 0;
			
			if(lego1.getSurface() < lego2.getSurface()){
				comparaison = -1;
			}else if(lego1.getSurface() > lego2.getSurface()){
				comparaison = 1;
			}else{
				comparaison =  Integer.compare(lego1.hauteur,lego2.hauteur);
			}
			
			return comparaison;
		}	
	}
	
	public Queue<BlocLego> enlignerLesBlocs(){
		Queue<BlocLego> file = new LinkedList<>(contenu);
		contenu.clear();
		return file;
	}	
	
	public void mettreBlocDansLaBoite(BlocLego bL){
		contenu.add(bL);
	}

	public BlocLego piger() {
		BlocLego bL = contenu.remove(RandomHelper.rand.nextInt(contenu.size()));
		return bL;
	}
	
	public static ArrayList<BlocLego> dataABloc(String[] data){
		ArrayList<BlocLego> blocs = new ArrayList<>(Integer.parseInt(data[0]));
		
		BlocLego bL;
		int h,l,p;
		String[] dataSplit;
		for(int i = 1;i < data.length; ++i){
			dataSplit = data[i].split(" ");
			
			h = Integer.parseInt(dataSplit[0]);
			l = Integer.parseInt(dataSplit[1]);
			p = Integer.parseInt(dataSplit[2]);
			
			bL = new BlocLego(h, l, p);
			blocs.add(bL);
			
			h = Integer.parseInt(dataSplit[1]);
			l = Integer.parseInt(dataSplit[0]);
			p = Integer.parseInt(dataSplit[2]);
			
			bL = new BlocLego(h, l, p);
			blocs.add(bL);
			
			h = Integer.parseInt(dataSplit[2]);
			l = Integer.parseInt(dataSplit[0]);
			p = Integer.parseInt(dataSplit[1]);
			
			bL = new BlocLego(h, l, p);
			blocs.add(bL);
		}
		
		return blocs;
		
	}

	public static boolean validerTour(BoiteALego boiteALego, ArrayList<BlocLego> legos) {
		boolean valide = true;
		for(BlocLego bL : legos){
			if(boiteALego.contenu.contains(bL) == false){
				System.out.println("IL MANQUE UN LEGO");
				valide = false;
				break;
			}
		}
		return valide;
	}
		
}
