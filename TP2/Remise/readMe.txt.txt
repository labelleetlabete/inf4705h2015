Pour lancer un exécutable, utilisez la commande suivante:
java -jar nomExecutable.jar -f cheminFichierDonnees -p

Notez bien que -p est optionnel et permet l'affichage de la composition de la tour.

Exemple:
java -jar vorace.jar -f C:\temp\inf4705h2015\TP2\Data\Dataset\b1k_6