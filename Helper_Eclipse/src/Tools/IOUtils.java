/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
//import java.util.stream.Stream;

/**
 *
 * @author Burn
 */
public class IOUtils {

    //http://howtodoinjava.com/2014/05/04/read-file-line-by-line-in-java-8-streams-of-lines-example/
    public static String[] fileToLines(String folderPath, String fileName) throws IOException {
        @SuppressWarnings("UnusedAssignment")
        String[] lines = new String[0];
        Path path = Paths.get(folderPath, fileName);
        
        
        BufferedReader reader = Files.newBufferedReader(path, Charset.defaultCharset());
        String line = null;
        List<String> linesList = new ArrayList<>();
        while ((line = reader.readLine()) != null) {
            linesList.add(line);
        }
        lines = new String[linesList.size()];
        lines = linesList.toArray(lines);
        

//        //The stream hence file will also be closed here
//        try (Stream<String> linesStream = Files.lines(path)) {
//            lines = linesStream.toArray(String[]::new);
//        }
        return lines;
    }
    
    public static void create(String folderPath, String fileName){
        try {
            Files.write(Paths.get(folderPath,fileName), "".getBytes("utf-8"),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING );
        } catch (IOException ex) {
            Logger.getLogger(IOUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void append(String folderPath, String fileName, String text){
        try {
            Files.write(Paths.get(folderPath,fileName), text.getBytes("utf-8"), StandardOpenOption.APPEND, StandardOpenOption.WRITE);
        } catch (IOException ex) {
            Logger.getLogger(IOUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
