/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sorters;

import Interfaces.ISortable;
import Tools.*;
import java.util.List;
/**
 *
 * @author bepala
 * @Source http://en.wikipedia.org/wiki/Selection_sort
 */
public class SelectionSort implements ISortable {

    @Override
    public long Sort(int[] arrayToSort) {
        Timer t = new Timer();
        t.Start();
        
        int i, j;
        int indexMin;

        /* advance the position through the entire array */
        /*   (could do j < n-1 because single element is also min element) */
        for (j = 0; j < arrayToSort.length - 1; j++) {
            /* find the min element in the unsorted a[j .. n-1] */

            /* assume the min is the first element */
            indexMin = j;
            /* test against elements after j to find the smallest */
            for (i = j + 1; i < arrayToSort.length; i++) {
                /* if this element is less, then it is the new minimum */
                if (arrayToSort[i] < arrayToSort[indexMin]) {
                    /* found new minimum; remember its index */
                    indexMin = i;
                }
            }

            if (indexMin != j) {
               ArrayUtils.swap(j,indexMin, arrayToSort);
            }

        }
        
        t.Stop();
        return t.getElapsedTimeMS();
    }

    void Sort(List<Integer> arrayToSort) {
                int i, j;
        int indexMin;

        /* advance the position through the entire array */
        /*   (could do j < n-1 because single element is also min element) */
        for (j = 0; j < arrayToSort.size() - 1; j++) {
            /* find the min element in the unsorted a[j .. n-1] */

            /* assume the min is the first element */
            indexMin = j;
            /* test against elements after j to find the smallest */
            for (i = j + 1; i < arrayToSort.size(); i++) {
                /* if this element is less, then it is the new minimum */
                if (arrayToSort.get(i) < arrayToSort.get(indexMin)) {
                    /* found new minimum; remember its index */
                    indexMin = i;
                }
            }

            if (indexMin != j) {
               ArrayUtils.swap(j,indexMin, arrayToSort);
            }

        }
    }

}
