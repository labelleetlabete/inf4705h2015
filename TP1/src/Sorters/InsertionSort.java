/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sorters;

import Interfaces.ISortable;
import Tools.Timer;
import java.util.List;

/**
 *
 * @author bepala
 */
public class InsertionSort implements ISortable {

    @Override
    public long Sort(int[] A) {

        Timer t = new Timer();
        t.Start();
        for (int i = 1; i < A.length; i++) {
            int value = A[i];
            int j = i - 1;
            while (j >= 0 && A[j] > value) {
                A[j + 1] = A[j];
                j = j - 1;
            }
            A[j + 1] = value;

        }
        t.Stop();

        return t.getElapsedTimeMS();
    }

    void Sort(List<Integer> A) {
        for (int i = 1; i < A.size(); i++) {
            int value = A.get(i);
            int j = i - 1;
            while (j >= 0 && A.get(j) > value) {
                A.set(j + 1,A.get(j));
                j = j - 1;
            }
            A.set(j + 1, value);

        }
    }
}
