package legos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import Tools.RandomHelper;

public class BoiteALego {
	public ArrayList<BlocLego> contenu;
	
	public BoiteALego(ArrayList<BlocLego> legos)
	{
		contenu = legos;
	}
	
	public void OrdonnerLego(){
		Collections.sort(contenu, new LegoComparator());
		Collections.reverse(contenu);
	}
	

	
	public Queue<BlocLego> enlignerLesBlocs(){
		Queue<BlocLego> file = new LinkedList<>(contenu);
		contenu.clear();
		return file;
	}	
	
	public void ajouterBloc(BlocLego bL){
		contenu.add(bL);
	}

	public BlocLego piger() {
		BlocLego bL = contenu.remove(RandomHelper.rand.nextInt(contenu.size()));
		return bL;
	}
	
	public static ArrayList<BlocLego> dataABloc(String[] data){
		ArrayList<BlocLego> blocs = new ArrayList<>(Integer.parseInt(data[0]));
		
		BlocLego bL;
		int h,l,p;
		String[] dataSplit;
		for(int i = 1;i < data.length; ++i){
			dataSplit = data[i].split(" ");
			
			h = Integer.parseInt(dataSplit[0]);
			l = Integer.parseInt(dataSplit[1]);
			p = Integer.parseInt(dataSplit[2]);
			
			bL = new BlocLego(h, l, p);
			blocs.add(bL);
		}
		
		return blocs;
		
	}
	
	private class LegoComparator implements Comparator<BlocLego>{

		@Override
		public int compare(BlocLego lego1, BlocLego lego2) {			
			int comparaison = 0;
			
			if(lego1.getSurface() < lego2.getSurface()){
				comparaison = -1;
			}else if(lego1.getSurface() > lego2.getSurface()){
				comparaison = 1;
			}else{
				//comparaison =  Integer.compare(lego1.profondeur - lego1.largeur,lego2.profondeur - lego2.largeur);
			}
			
			return comparaison;
		}	
	}

	public boolean validerTours(List<Tour> tours) {
		boolean isOk = true;
		
		ArrayList<BlocLego> contenuCopie = copyContenu();
		
		for(Tour t : tours){
			
			
			isOk = Tour.validerTour(t);
			
			if(!isOk){
				break;
			}
			
			for(BlocLego bl : t.blocs){
				isOk = contenuCopie.remove(bl);
				
				if(!isOk){
					break;
				}
			}
			
			if(!isOk){
				break;
			}			
		}
		
		if(isOk){ //au cas si on a trop de bloc dans nos tours
			isOk = contenuCopie.isEmpty();
		}
		
		return isOk;		
	}

	private ArrayList<BlocLego> copyContenu() {
		ArrayList<BlocLego> copie = new ArrayList<>();
		
		for(BlocLego bl : contenu){
			copie.add(bl);
		}
		
		return copie;
	}
		
}
