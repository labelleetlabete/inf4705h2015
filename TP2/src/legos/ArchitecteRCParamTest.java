package legos;

import java.io.IOException;

import Tools.IOUtils;
import Tools.RandomHelper;
import algoritmes.ConstructeurDynamique;
import algoritmes.ConstructeurRecuitSimule;
import algoritmes.ConstructeurTourLego;
import algoritmes.ConstructeurVorace;

public class ArchitecteRCParamTest {
	public void Archi() throws IOException {
		int nbPas = 728;
		String folderPath = "Data/Dataset";
		String resultatsHauteur = "resultatsHauteur.txt";
		String folderResultat = "Data/Resultats/Dynamique";
		IOUtils.create(folderResultat, resultatsHauteur);

		String[] grosseurs = new String[] { "100", "500", "1k", "5k", "10k",
				"50k", "100k" };
		int[] grosseursInt = new int[] { 100, 500, 1000, 5000, 10000, 50000,
				100000 };
		String data[];
		BoiteALego bAL;
		Tour t;
		long duree;
		int hauteur;

		int i = 0;
		for (String grosseurS : grosseurs) {
			data = IOUtils.fileToLines(folderPath, "b" + grosseurS + "_" + 0);

			resultatsHauteur = "resultats_" + grosseurS + ".txt";
			IOUtils.create(folderResultat, resultatsHauteur);
			for (int pallier = 0; pallier <= grosseursInt[i] * 3; pallier += 0.25 * grosseursInt[i]) {
				bAL = new BoiteALego(BoiteALego.dataABloc(data));
				ConstructeurRecuitSimule constructeur = new ConstructeurRecuitSimule(
						false, nbPas, 150, pallier, 0.99f);
				t = constructeur.Construire(bAL);

				IOUtils.append(folderResultat, resultatsHauteur, pallier + " "
						+ t.getHauteur() + System.lineSeparator());
			}

			++i;
		}
	}
}
