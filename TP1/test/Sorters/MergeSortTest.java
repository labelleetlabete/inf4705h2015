/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sorters;

import Tools.ArrayUtils;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bepala
 */
public class MergeSortTest {

    MergeSort sorter;

    @Before
    public void setUp() {
        sorter = new MergeSort();
    }

    /**
     * Test of Sort method, of class SelectionSort.
     */
    @Test
    public void sortRandom() {
        MergeSort.THRESHOLD =1;
        int[] array = {
            6442,
            1883,
            579,
            5580,
            3728,
            1194,
            3542,
            7390,
            7854,
            1915,
            1427,
            7286,
            7855,
            1008,
            1798,
            1537,
            1212,
            4964,
            4144,
            5099,
            1176,
            1792,
            9697,
            1744,
            1197,
            1650,
            5955,
            7332};

        sorter.Sort(array);
        System.out.println("*******");
        ArrayUtils.printArray(array);
        assertTrue(ArrayUtils.isAscending(array));

    }

    /**
     * Test of Sort method, of class SelectionSort.
     */
    @Test
    public void sortWorstCase() {
         MergeSort.THRESHOLD =1;
        int[] array = {
            999,
            888,
            777,
            666,
            555,
            444,
            333,
            222,
            111,
            99,
            88,
            77};

        sorter.Sort(array);
        System.out.println("*******");
        ArrayUtils.printArray(array);
        assertTrue(ArrayUtils.isAscending(array));
    }

    /**
     * Test of Sort method, of class SelectionSort.
     */
    @Test
    public void testBestCase() {
         MergeSort.THRESHOLD =1;
        int[] array = {
            1,
            2,
            3,
            4,
            5,
            6,
            5656,
            88888,
            999999,
            9999999};

        sorter.Sort(array);
        System.out.println("*******");
        ArrayUtils.printArray(array);
        assertTrue(ArrayUtils.isAscending(array));
    }
    
        /**
     * Test of Sort method, of class SelectionSort.
     */
    @Test
    public void sortRandomThres() {
        MergeSort.THRESHOLD =4;
        int[] array = {
            6442,
            1883,
            579,
            5580,
            3728,
            1194,
            3542,
            7390,
            7854,
            1915,
            1427,
            7286,
            7855,
            1008,
            1798,
            1537,
            1212,
            4964,
            4144,
            5099,
            1176,
            1792,
            9697,
            1744,
            1197,
            1650,
            5955,
            7332};

        sorter.Sort(array);
        System.out.println("*******");
        ArrayUtils.printArray(array);
        assertTrue(ArrayUtils.isAscending(array));

    }

    /**
     * Test of Sort method, of class SelectionSort.
     */
    @Test
    public void sortWorstCaseThres() {
         MergeSort.THRESHOLD =4;
        int[] array = {
            999,
            888,
            777,
            666,
            555,
            444,
            333,
            222,
            111,
            99,
            88,
            77};

        sorter.Sort(array);
        System.out.println("*******");
        ArrayUtils.printArray(array);
        assertTrue(ArrayUtils.isAscending(array));
    }

    /**
     * Test of Sort method, of class SelectionSort.
     */
    @Test
    public void testBestCaseThres() {
         MergeSort.THRESHOLD =4;
        int[] array = {
            1,
            2,
            3,
            4,
            5,
            6,
            5656,
            88888,
            999999,
            9999999};

        sorter.Sort(array);
        System.out.println("*******");
        ArrayUtils.printArray(array);
        assertTrue(ArrayUtils.isAscending(array));
    }

}
