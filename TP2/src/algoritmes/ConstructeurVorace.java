package algoritmes;

import java.util.Queue;

import Tools.Timer;
import legos.BlocLego;
import legos.BoiteALego;
import legos.Tour;

public class ConstructeurVorace extends ConstructeurTourLego {

	public ConstructeurVorace(boolean verbose) {
		super(verbose);
	}

	@Override
	public Tour Construire(BoiteALego boite) {
		Timer time = new Timer();
		time.Start();

		boite.OrdonnerLego();

		Queue<BlocLego> file = boite.enlignerLesBlocs();

		Tour t = new Tour();
		BlocLego pretendant;
		while (file.isEmpty() == false) {
			pretendant = file.remove();
			if (!t.ajouterBlocLego(pretendant)) {
				boite.mettreBlocDansLaBoite(pretendant);
			}
		}

		time.Stop();
		t.tempsDeConstruction = time.getElapsedTimeMicroS();

		return t;
	}

}
