package test.constructeurs;

import static org.junit.Assert.*;

import java.io.IOException;

import legos.BoiteALego;
import legos.Tour;

import org.junit.Test;

import Tools.IOUtils;
import algoritmes.ConstructeurDynamique;
import algoritmes.ConstructeurRecuitSimule;
import algoritmes.ConstructeurTourLego;

public class ConstructeurRecuitSimuleTest {

	@Test
	public void test() {
		String folderPath = "donnees";
		String fileName = "b1k_0";
		
		String[] data =null;
		try {
			data = IOUtils.fileToLines(folderPath, fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BoiteALego bAL = new BoiteALego(BoiteALego.dataABloc(data));
		
		ConstructeurTourLego constructeur = new ConstructeurRecuitSimule(true,
				(int)Math.sqrt(bAL.contenu.size() /2),
				100f,
				(int)Math.sqrt(bAL.contenu.size() /2), //?
				0.99f);
		Tour t = constructeur.Construire(bAL);
		
		t.decrisToi(true);

		assertTrue(Tour.validerTour(t));
		assertTrue(BoiteALego.validerTour(new BoiteALego(BoiteALego.dataABloc(data)),t.blocs));
	}

}
