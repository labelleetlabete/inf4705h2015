package algoritmes;

import Tools.Timer;
import legos.BlocLego;
import legos.BoiteALego;
import legos.Tour;

public class ConstructeurDynamique extends ConstructeurTourLego {

	
	public ConstructeurDynamique(boolean verbose){
		super(verbose);
	}
	
	//On a utilise http://www.geeksforgeeks.org/dynamic-programming-set-21-box-stacking-problem comme exemple et base pour notre code
	@Override
	public Tour Construire(BoiteALego boite) {
		Timer time = new Timer();
		time.Start();


		boite.OrdonnerLego();

		
		BlocLego[] blocs = new BlocLego[boite.contenu.size()];
		boite.contenu.toArray(blocs);
		Tour[] tours = new Tour[blocs.length];
		
	//on initialise la hauteur maximale a la hauteur du dit blocs
	   //int[] sousHauteurMax = new int[blocs.length];
	   for (int i = 0; i < blocs.length; i++ ){
		   //sousHauteurMax[i] = blocs[i].hauteur;
		   tours[i] = new Tour();
		   tours[i].ajouterBlocLego(blocs[i]);
	   }
	 
	   //on calcule la sous hauteur maximale avec le bloc i au top
	   for (int i = 1; i < blocs.length; i++ ){
	      for (int j = 0; j < i; j++ ){
	    	  //on regarde si on peut le mettre par dessus et si le rajouter est plus grand que la sous solution a i 
	         if ( blocs[i].largeur < blocs[j].largeur &&
	        		 blocs[i].profondeur < blocs[j].profondeur &&
	        		 tours[i].getHauteur() < tours[j].getHauteur() + blocs[i].hauteur
	            )
	         {
	        	 tours[i].remplacerBase(tours[j]);
	         }
	      }
	   }
	 
	 
	   /* Pick maximum of all msh values */
	   int max = -1;
	   Tour tourGagnante = null;
	   for ( int i = 0; i < blocs.length; i++ ){
	      if ( max < tours[i].getHauteur() ){
	         max = tours[i].getHauteur();
	         tourGagnante = tours[i];
	      }
	   }
	   time.Stop();
	   tourGagnante.tempsDeConstruction = time.getElapsedTimeMicroS();
	   return tourGagnante;

	}

}
