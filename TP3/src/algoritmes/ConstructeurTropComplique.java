package algoritmes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import legos.BlocLego;
import legos.BoiteALego;
import legos.Tour;

public class ConstructeurTropComplique extends ConstructeurToursLego {

	public ConstructeurTropComplique(boolean verbose) {
		super(verbose);
		
	}

	@Override
	public List<Tour> Construire(BoiteALego boite) {
		int blocCount = 0;
		LinkedList<Tour> tours = new LinkedList<>();
		
		ArrayList<ArrayList<BlocLego>> blocsLargeur = new ArrayList<ArrayList<BlocLego>>();
		ArrayList<ArrayList<BlocLego>> blocsProfondeur = new ArrayList<ArrayList<BlocLego>>();
		
		for(int i =0; i < 150; ++i){
			blocsLargeur.add(new ArrayList<BlocLego>());
			blocsProfondeur.add(new ArrayList<BlocLego>());
		}

		
		for(BlocLego bloc : boite.contenu){
			blocsLargeur.get(bloc.largeur).add(bloc);
			blocsProfondeur.get(bloc.profondeur).add(bloc);
		}
		
		Tour tourCourante;
		BlocLego bloc;
		while(blocCount != boite.contenu.size()){
			tourCourante = new Tour();
			//on trouve celui avec la plus grand profondeur
			for(int i = blocsProfondeur.size() -1; i >= 0; --i){
				if(blocsProfondeur.get(i).isEmpty() == false){
					
					for(int j = 0;j < blocsProfondeur.get(i).size(); ++j){	
						bloc = blocsProfondeur.get(i).get(j);
						if(tourCourante.ajouterBlocLego(bloc)){
							blocsLargeur.get(bloc.largeur).remove(bloc);
							blocsProfondeur.get(bloc.profondeur).remove(bloc);
							i = tourCourante.top.largeur;
							++blocCount;
							break;
						}
					}

				}
			}
			tours.add(tourCourante);
		}
				
		return tours;
	}

}
