package Architectes;

import java.io.IOException;
import java.util.List;

import legos.BoiteALego;
import legos.Tour;
import Tools.IOUtils;
import algoritmes.ConstructeurParesseux;
import algoritmes.ConstructeurToursLego;
import algoritmes.ConstructeurTropComplique;

public class ArchitecteComplique implements IArchitecte {

	
	
	public void archi() {

		ConstructeurToursLego cTL = new ConstructeurTropComplique(true);
		
		String folderPath = "Data/Dataset";
		String resultatsHauteur = "resultats.txt";
		String folderResultat = "Data/Resultats/Complique";
		IOUtils.create(folderResultat, resultatsHauteur);

		String[] grosseurs = new String[] { "100", "500", "1k", "5k",
				"10k", "50k", "100k" };
		int[] grosseursInt = new int[] { 100, 500, 1000, 5000,
				10000, 50000, 100000 };
		String data[];
		BoiteALego bAL;
		List<Tour> t = null;
		
		float count = 0;
		int i =0;
		for (String grosseur : grosseurs) {
			count = 0;
			for (int set = 0; set < 10; ++set) {

				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseur + "_"
							+ set);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					
				    t = cTL.Construire(bAL);
				    count += t.size();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			IOUtils.append(folderResultat, resultatsHauteur, grosseursInt[i] + " " + count / 10f + System.lineSeparator());
			++i;
		}
	}

}
