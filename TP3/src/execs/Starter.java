package execs;

import java.io.IOException;
import java.util.List;

import algoritmes.ConstructeurParesseux;
import algoritmes.ConstructeurToursLego;
import legos.BlocLego;
import legos.BoiteALego;
import legos.Tour;
import Tools.IOUtils;


public class Starter {

	public static void main(String[] args) {
		boolean verbose = false;
		String emplacement ="";
		if(args.length > 1){
			for(int i =0; i < args.length; ++i){
				String arg = args[i];
				
				switch(arg){
				case "-p":
					verbose = true;
					break;
				case "-f":
					emplacement = args[i+1];
					++i;
					break;
				default:
				}
				

				
			}
			String data[];
			try {
				data = IOUtils.fileToLines(emplacement, "");
				BoiteALego bAL = new BoiteALego(BoiteALego.dataABloc(data));				
				ConstructeurToursLego constructeur = new ConstructeurParesseux(false);
				
			    List<Tour> tours = constructeur.Construire(bAL);
			    
			    System.out.println(tours.size());
			    if(verbose){
			    	for(Tour t : tours){
			    		System.out.println(t.blocs.size());
			    		for(BlocLego bl : t.blocs){
			    			System.out.println(String.format("%d\t\t%d\t\t%d", bl.largeur,bl.profondeur,bl.hauteur));
			    		}
			    	}
			    }
			    
			    
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			System.out.println("Il vous manque des arguments");
		}

	}

}
