package legos;

import java.io.IOException;

import Tools.IOUtils;
import Tools.RandomHelper;
import algoritmes.ConstructeurRecuitSimule;
import algoritmes.ConstructeurTourLego;
import algoritmes.ConstructeurVorace;

public class ArchitecteVorace {
	public void Archi() {
		String folderPath = "Data/Dataset";
		String resultatsHauteur = "resultats.txt";
		String folderResultat = "Data/Resultats/Vorace";
		IOUtils.create(folderResultat, resultatsHauteur);

		String[] grosseurs = new String[] { "100", "500", "1k", "5k",
				"10k", "50k", "100k" };
		int[] grosseursInt = new int[] { 100, 500, 1000, 5000,
				10000, 50000, 100000 };
		String data[];
		BoiteALego bAL;
		Tour t;
		long duree;
		int hauteur;
		
		for (String grosseur : grosseurs) {
			hauteur =0;
			duree =0;
			for (int set = 0; set < 10; ++set) {

				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseur + "_"
							+ set);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					ConstructeurVorace constructeur = new ConstructeurVorace(false);
				    t = constructeur.Construire(bAL);
				    hauteur += t.getHauteur();
				    duree += t.tempsDeConstruction;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		
		int i =0;
		for (String grosseur : grosseurs) {
			hauteur =0;
			duree =0;
			for (int set = 0; set < 10; ++set) {

				try {
					data = IOUtils.fileToLines(folderPath, "b" + grosseur + "_"
							+ set);
					bAL = new BoiteALego(BoiteALego.dataABloc(data));
					ConstructeurVorace constructeur = new ConstructeurVorace(false);
				    t = constructeur.Construire(bAL);
				    hauteur += t.getHauteur();
				    duree += t.tempsDeConstruction;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			IOUtils.append(folderResultat, resultatsHauteur, grosseursInt[i] + " " + hauteur / 10 + " " + duree / 10 + System.lineSeparator());
			++i;
		}
	}
}
