package algoritmes;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import legos.BlocLego;
import legos.BoiteALego;
import legos.Tour;

public class ConstructeurParesseux extends ConstructeurToursLego {

	public ConstructeurParesseux(boolean verbose) {
		super(verbose);

	}

	@Override
	public List<Tour> Construire(BoiteALego boite) {
		LinkedList<Tour> tours = new LinkedList<>();
		Tour t = new Tour();

		boite.OrdonnerLego();
		tours.add(t);
		Iterator<Tour> it;
		int meilleurSurface = Integer.MAX_VALUE;
		Tour meilleurTour;

		for (BlocLego bloc : boite.contenu) {
			meilleurTour = null;
			meilleurSurface = Integer.MAX_VALUE;

			it = tours.iterator();
			while (it.hasNext()) {
				t = it.next();
				if (t.peutSupporter(bloc)) {
					if (t.surfaceTop() < meilleurSurface) {
						meilleurSurface = t.surfaceTop();
						meilleurTour = t;
					}
				}

			}

			if (meilleurTour == null) {
				// on n'a pas ajoute le bloc, on cree une nouvelle tour avec le
				// bloc
				t = new Tour();
				t.ajouterBlocLego(bloc);
				tours.add(t);
			} else {
				meilleurTour.ajouterBlocLego(bloc);
			}
		}

		return tours;
	}

}
