package algoritmes;

import java.util.Random;

import javax.crypto.spec.OAEPParameterSpec;

import Tools.RandomHelper;
import Tools.Timer;
import legos.BlocLego;
import legos.BoiteALego;
import legos.Tour;

public class ConstructeurRecuitSimule extends ConstructeurTourLego {

	int pasMax;
	float tempInitiale;
	int pallierDeRefroidissement;
	float coeff;
	
	public ConstructeurRecuitSimule(boolean verbose, int pasMax, float tempInitiale, int pallierDeRefroidissement, float alpha) {
		super(verbose);
		this.pasMax = pasMax;
		this.tempInitiale = tempInitiale;
		this.pallierDeRefroidissement = pallierDeRefroidissement;
		this.coeff = alpha;
	}

	@Override
	public Tour Construire(BoiteALego boite) {
		Timer time = new Timer();
		time.Start();
		
		Tour tour0 = ConstruireTourInitiale(boite); //la boite ne contient plus les blocs de la tour0
		Tour tourTemp = tour0;
		Tour meilleureTour = tour0;
		float temperature = tempInitiale;
		
		for(int i =0; i < pasMax; ++i){
			for(int j = 1; j < pallierDeRefroidissement;++j){
				Tour tourNouveau = voisin(tourTemp,boite);

				int deltaHauteur = tourNouveau.getHauteur() - tourTemp.getHauteur();
				if(CritereMetropolis(deltaHauteur,temperature)){
					boite.contenu.addAll(tourNouveau.blocsInstables);
					tourNouveau.blocsInstables.clear();

					tourTemp = tourNouveau;

					if(tourTemp.getHauteur() - meilleureTour.getHauteur() > 0){
						meilleureTour = tourTemp;
					}
				}else{
					boite.mettreBlocDansLaBoite(tourNouveau.derniereInsertion);
				}
				tourTemp.blocsInstables.clear();
				meilleureTour.blocsInstables.clear();
			}
			temperature = temperature * coeff;
		}
		
		time.Stop();
		meilleureTour.tempsDeConstruction = time.getElapsedTimeMicroS();
		return meilleureTour;
	}


	private boolean CritereMetropolis(int deltaHauteur, float temperature) {
		if(deltaHauteur >= 0){
			return true;
		}
		if(Math.pow(Math.E, (double)deltaHauteur/(double)temperature) >= RandomHelper.rand.nextDouble()){
			return true;
		}else{
			return false;
		}
	}

	private Tour voisin(Tour tourTemp,BoiteALego boite) {
		BlocLego bL = boite.piger();
		Tour tourVoisin = new Tour(tourTemp);
		tourVoisin.inserer(bL,boite);
		return tourVoisin;
	}

	private Tour ConstruireTourInitiale(BoiteALego boite) {
		ConstructeurVorace cV = new ConstructeurVorace(false);
		return cV.Construire(boite);	
	}
	


}
